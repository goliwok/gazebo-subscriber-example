# gazebo-subscriber-example

used with this project:

https://darienmt.com/autonomous-flight/2018/11/15/installing-ethz-rotors.html

perform all the steps in it before used this catkin_ws.
to build the two binaries, execute this command in catkin_ws root:

`catkin_make`

make sure you use the right env

when the build is completed, you should get two binaries in :

~/catkin_ws/devel/lib/dolos/

You need to start the Ethz gazebo's project before running one of these binaries.
Use the following command in the folder you create for Ethz project (~/project/):

`roslaunch rotors_gazebo mav_hovering_example.launch mav_name:=firefly world_name:=basic`


