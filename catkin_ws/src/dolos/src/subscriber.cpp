#include "ros/ros.h"
#include "std_msgs/String.h"

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void dolosCb(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("dolos info from publisher -> [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "dolos_subscriber");
  ros::NodeHandle node;
  ros::Subscriber dolos_subscriber = node.subscribe("dolos_publisher", 1000, dolosCb);
  ros::spin();
  return 0;
}