#include <iomanip>
#include <sstream>
#include <ros/ros.h>
#include <ros/console.h>
#include <boost/asio.hpp>
#include "std_msgs/String.h"

using boost::asio::ip::tcp;

double HZ_LOOP_RATE = 5.0;
int LIMIT = 300;

int main(int argc, char** argv) {
    ros::init(argc, argv, "dolos");
    ros::start();
    ROS_DEBUG("dolos start"); /*  /!\ lancer rqt_console pour voir ces logs ! (https://wiki.ros.org/rosconsole)  */
    ros::NodeHandle node;

    ROS_INFO_STREAM("dolos ros package initialized: " << std::boolalpha << ros::isInitialized());
    ROS_INFO_STREAM("prepare next step ...");
    ROS_INFO_STREAM("node status : " << std::boolalpha << ros::ok());
    ROS_INFO_STREAM("node handle status : " << std::boolalpha << node.ok());
    ROS_INFO_STREAM("loop rate set to : " << HZ_LOOP_RATE << " Hz (" << 1/HZ_LOOP_RATE << " sec)");

    ros::Publisher dolos_publisher = node.advertise<std_msgs::String>("dolos_publisher", 1000);
    ros::Rate loop_rate(HZ_LOOP_RATE);
    
    for (int count = 0; count <= LIMIT && ros::ok(); count++)
    { 
        std_msgs::String msg;
        std::stringstream ss;
        ss << "dolos ping ... " << count;
        msg.data = ss.str();
        ROS_INFO_STREAM(msg.data);
        ROS_DEBUG_STREAM(msg.data);
        dolos_publisher.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();
    }

    ros::shutdown();
    return 0;
}